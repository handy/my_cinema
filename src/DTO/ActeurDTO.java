package DTO;

import java.io.Serializable;
import java.util.Date;

public class ActeurDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private int nacteur;
    private String nom;
    private String prenom;
    private java.util.Date naissance;
    private String nationalite;
    private int nbrFilms;

    public int getNacteur() {
        return nacteur;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public int getNbrFilms() {
        return nbrFilms;
    }

    public ActeurDTO(int nacteur, String nom, String prenom, Date naissance, String nationalite, int nbrFilms) {
        this.nacteur = nacteur;
        this.nom = nom;
        this.prenom = prenom;
        this.naissance = naissance;
        this.nationalite = nationalite;
        this.nbrFilms = nbrFilms;
    }

    @Override
    public String toString() {
        return "ActeurDTO{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", nationalite='" + nationalite + '\'' +
                ", nbrFilms=" + nbrFilms +
                "}\n";
    }
}
