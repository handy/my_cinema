package DTO;

public class ClientDTO {
    int nclient;
    String nom;
    String prenom;
    String adresse;
    int ancienete;

    public int getNclient() {
        return nclient;
    }

    public void setNclient(int nclient) {
        this.nclient = nclient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getAncienete() {
        return ancienete;
    }

    public void setAncienete(int ancienete) {
        this.ancienete = ancienete;
    }

    public ClientDTO(int nclient, String nom, String prenom, String adresse, int ancienete) {
        this.nclient = nclient;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.ancienete = ancienete;
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
                "nclient=" + nclient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", ancienete=" + ancienete +
                '}';
    }
}
