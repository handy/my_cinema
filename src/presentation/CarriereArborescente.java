package presentation;

import DTO.FilmDTO;
import DTO.GenreDTO;
import metier.DAOMetier;
import metier.IDAOMetier;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

public class CarriereArborescente {

    private IDAOMetier cinema;


    private JPanel RootPanel;
    private JTree treeGenre;
    private JTextArea textAreaInfo;

    private TreeModel creationModele(){
        DefaultMutableTreeNode racine =
                new DefaultMutableTreeNode("genre");
        try {
            for (GenreDTO g : cinema.ensGenres()) {
                DefaultMutableTreeNode noeud =
                        new DefaultMutableTreeNode(g);
                racine.add(noeud);
                for (FilmDTO f : cinema.ensFilmDuGenre(g.getNgenre())) {
                    noeud.add(new DefaultMutableTreeNode(f));
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return new DefaultTreeModel(racine);
    }

    private void init(){
        try {
            cinema = new DAOMetier().getInstance();
        }catch (Exception e) {
            System.err.println(e);
        }
    }

    public CarriereArborescente() {
        init();
        treeGenre.setModel(creationModele());
        treeGenre.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        treeGenre.addTreeExpansionListener(new TreeExpansionListener() {
            @Override
            public void treeExpanded(TreeExpansionEvent event) {
                try {
                    DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
                    Object obj =  noeud.getUserObject();
                    if(obj instanceof GenreDTO){
                        GenreDTO genre = (GenreDTO) obj;
                        textAreaInfo.setText("Nombre de film du genre " + genre + " : "+ cinema.nbreFilmDuGenre(genre.getNgenre()));
                    }

                } catch (Exception e){ e.printStackTrace(); }
            }

            @Override
            public void treeCollapsed(TreeExpansionEvent event) {

            }
        });
        treeGenre.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                try {
                    DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
                    Object obj =  noeud.getUserObject();
                    if(obj instanceof GenreDTO){
                        GenreDTO genre = (GenreDTO) obj;
                        textAreaInfo.setText("Nombre de film du genre " + genre + " : "+ cinema.nbreFilmDuGenre(genre.getNgenre()));
                    }
                } catch (Exception er){ er.printStackTrace(); }
                if (treeGenre.getSelectionPath() != null && treeGenre.getSelectionPath().getPathCount() == 3) {
                    DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) treeGenre.getLastSelectedPathComponent();
                    if (noeud.isLeaf()) {
                        try {
                            Object obj =  noeud.getUserObject();
                            System.out.println(obj instanceof FilmDTO);
                            if(obj instanceof FilmDTO) {
                                FilmDTO film = (FilmDTO) obj;
                                textAreaInfo.setText("Information sur le film : " + film.getTitre()
                                        + "\nRéalisateur : " + film.getNom_realisateur()
                                        + "\nActeur principal : " + film.getNom_acteur());
                            }
                        } catch (Exception ex) { ex.printStackTrace();}
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("CarriereArborescente");
        frame.setContentPane(new CarriereArborescente().RootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
