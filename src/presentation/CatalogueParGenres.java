package presentation;

import DTO.FilmDTO;
import DTO.GenreDTO;
import metier.DAOMetier;
import metier.IDAOMetier;


import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.List;

public class CatalogueParGenres {

    private IDAOMetier cinema;

    private JPanel rootPanel;
    private JList<GenreDTO> listGenre;
    private JList<FilmDTO> listFilmDuGenre;
    private JTextArea textAreaNbFilmsDuGenre;
    private JTextArea textAreaInfoFilm;

    private Color gris = new Color(0xdbd5b5);
    private Color grisC = new Color(0xfff8d3);
    private Color blue = new Color(0x2b9eb3);

    private void init() {

        this.rootPanel.setBackground(gris);
        this.listGenre.setBackground(grisC);
        this.listFilmDuGenre.setBackground(grisC);
        this.textAreaNbFilmsDuGenre.setBackground(grisC);
        this.textAreaInfoFilm.setBackground(grisC);
        this.listGenre.setForeground(blue);
        this.listFilmDuGenre.setForeground(blue);
        this.textAreaNbFilmsDuGenre.setForeground(blue);
        this.textAreaInfoFilm.setForeground(blue);

        try {
            cinema = DAOMetier.getInstance();
            final List<GenreDTO> genres = cinema.ensGenres();

            DefaultListModel<GenreDTO> lm = new DefaultListModel<>();
            for (GenreDTO unGenre : genres) {
                lm.addElement(unGenre);
            }
            listGenre.setModel(lm);

            this.textAreaNbFilmsDuGenre.setText("Nbr de films du genre :");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public CatalogueParGenres() {
        init();
        listGenre.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                try {
                    GenreDTO selection = listGenre.getSelectedValue();

                    textAreaNbFilmsDuGenre.setText("Nbr de film du genre\n" + selection + " : " + cinema.nbreFilmDuGenre(selection.getNgenre()));

                    textAreaInfoFilm.setText("");

                    final List<FilmDTO> films = cinema.ensFilmDuGenre(selection.getNgenre());
                    ListModel<FilmDTO> lm = new AbstractListModel<>() {

                        @Override
                        public int getSize() {
                            return films.size();
                        }

                        @Override
                        public FilmDTO getElementAt(int index) {
                            return films.get(index);
                        }
                    };
                    listFilmDuGenre.setModel(lm);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        listFilmDuGenre.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                FilmDTO selection = listFilmDuGenre.getSelectedValue();
                if (selection != null) {
                    textAreaInfoFilm.setText("Acteur principal : " + selection.getNom_acteur());
                    textAreaInfoFilm.append("\nRéalisateur : " + selection.getNom_realisateur());
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("CatalogueParGenres");
        frame.setContentPane(new CatalogueParGenres().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
