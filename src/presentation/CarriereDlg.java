package presentation;

import metier.ScrollingResultSetTableModel;
import service.ConnectCinema;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarriereDlg extends JDialog {
    private JPanel RootPanel;
    private JButton buttonOK;
    private JTable tableCarriere;

    private PreparedStatement pstmt;
    private ResultSet rs;
    private ScrollingResultSetTableModel model;

    private static String QUERY =
            "SELECT TITRE, NATURE, nom as PAYS, REALISATEUR, SORTIE, ENTREES " +
                    "FROM FILM " +
                    "JOIN GENRE USING(ngenre) " +
                    "JOIN PAYS USING(npays) " +
                    "WHERE NACTEURPRINCIPAL = ? " +
                    "ORDER BY TITRE";

    private void init() {
        try {
            pstmt =ConnectCinema.getInstance().prepareStatement(QUERY,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        }catch (SQLException e){
            System.err.println(e);
        }
    }


    public CarriereDlg(){
        init();
        setContentPane(RootPanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
    }

    private void onOK() {
        this.setVisible(false);
    }

    public void show(int nActeur){
        try {
            if (rs != null) {
                rs.close();
            }
            pstmt.setInt(1 , nActeur);
            rs = pstmt.executeQuery();

            model = new ScrollingResultSetTableModel(rs);
            this.tableCarriere.setModel(model);

            this.setVisible(true);
        }catch (SQLException e){
            System.err.println(e);
        }
    }


    public static void main(String[] args){
        CarriereDlg dialog = new CarriereDlg();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
