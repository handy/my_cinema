package presentation;

import DTO.ActeurDTO;
import metier.Navigation_Acteur;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class CineActeur {

    private JPanel rootPanel;
    private JButton buttonFirst;
    private JButton buttonPrevious;
    private JProgressBar progressBarActeur;
    private JButton buttonNext;
    private JButton buttonLast;
    private JTextField textFieldNom;
    private JTextField textFieldPrenom;
    private JButton ButtonCarriere;
    private JPanel JPanelButton;
    private JPanel JPanelTextField;
    private JLabel JLabelNom;
    private JLabel JLabelPrenom;

    private Navigation_Acteur cinema;
    private ActeurDTO acteur = null;
    private int nacteur = 0;
    private CarriereDlg carriereDlg;

    private Color gris = new Color(0xdbd5b5);
    private Color grisC = new Color(0xfff8d3);
    private Color blue = new Color(0x2b9eb3);

    private void init() throws SQLException {

        buttonFirst.setIcon(new javax.swing.ImageIcon("/home/handy/Projets/myCinema/src/presentation/first.GIF"));
        buttonPrevious.setIcon(new javax.swing.ImageIcon("/home/handy/Projets/myCinema/src/presentation/previous.GIF"));
        buttonNext.setIcon(new javax.swing.ImageIcon("/home/handy/Projets/myCinema/src/presentation/next.GIF"));
        buttonLast.setIcon(new javax.swing.ImageIcon("/home/handy/Projets/myCinema/src/presentation/last.GIF"));
        this.rootPanel.setBackground(grisC);
        this.JPanelButton.setBackground(grisC);
        this.JPanelTextField.setBackground(grisC);
        this.textFieldNom.setForeground(blue);
        this.textFieldPrenom.setForeground(blue);
        this.ButtonCarriere.setBackground(gris);
        this.buttonFirst.setBackground(gris);
        this.buttonPrevious.setBackground(gris);
        this.buttonNext.setBackground(gris);
        this.buttonLast.setBackground(gris);
        this.progressBarActeur.setBackground(grisC);
        this.progressBarActeur.setForeground(gris);

        this.cinema = new Navigation_Acteur();

        this.progressBarActeur.setMaximum(cinema.getNbActeur());
        acteur = cinema.getFirstActeur();
        infoActeurBD();

    }

    private void infoActeurBD() {
        try {
            nacteur = acteur.getNacteur();
            this.textFieldNom.setText(acteur.getNom());
            this.textFieldPrenom.setText(acteur.getPrenom());

            this.progressBarActeur.setValue(cinema.position());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public CineActeur() throws SQLException {
        init();
        buttonNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    acteur = cinema.getNextActeur();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                if (acteur!=null)
                    infoActeurBD();
            }
        });
        buttonPrevious.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    acteur = cinema.getPreviousActeur();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                if (acteur!=null)
                    infoActeurBD();
            }
        });
        buttonFirst.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    acteur = cinema.getFirstActeur();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                infoActeurBD();
            }
        });
        buttonLast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    acteur = cinema.getLastActeur();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                infoActeurBD();
            }
        });
        ButtonCarriere.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (carriereDlg == null){
                    carriereDlg = new CarriereDlg();
                    carriereDlg.setSize(400,300);
                }
                carriereDlg.show(nacteur);
            }
        });
    }

    public static void main(String[] args) throws SQLException {
        JFrame frame = new JFrame("CineActeur");
        frame.setContentPane(new CineActeur().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
