package metier;

import DTO.ActeurDTO;
import service.ConnectCinema;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Navigation_Acteur {

    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet acteurRs;
    private int nbActeur;

    private final static String SQLSelectActeurs =
            "SELECT nacteur, a.nom AS nom, prenom, naissance ,p.nom AS nationalité, nbrefilms "
                    + "FROM ACTEUR a "
                    + "INNER JOIN PAYS p "
                    + "ON nationalite = npays "
                    + " ORDER BY a.nom, prenom";

    public Navigation_Acteur(){
        try {
            ouvrirConnection();

            acteurRs.last();
            nbActeur=acteurRs.getRow();

            acteurRs.first();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void ouvrirConnection() throws SQLException{

        this.conn = ConnectCinema.getInstance();

        this.stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        this.acteurRs = stmt.executeQuery(SQLSelectActeurs);

    }

    public void fermerConnection() throws SQLException{

        if (this.stmt != null){
            stmt.close();
        }
        if (this.conn != null){
            conn.close();
        }
    }

    public ActeurDTO getFirstActeur() throws SQLException {
        if (acteurRs == null)
            return null;
        this.acteurRs.first();
        return getActeurDTO();
    }

    public ActeurDTO getLastActeur() throws SQLException {
        if (acteurRs == null)
            return null;
        this.acteurRs.last();
        return getActeurDTO();
    }

    public ActeurDTO getNextActeur() throws SQLException {
        if (this.acteurRs == null)
            return null;
        if (this.acteurRs.isLast())
            return null;

        this.acteurRs.next();
        return getActeurDTO();
    }

    public ActeurDTO getPreviousActeur() throws SQLException {
        if (this.acteurRs == null)
            return null;
        if (this.acteurRs.isFirst())
            return null;

        this.acteurRs.previous();
        return getActeurDTO();
    }

    private ActeurDTO getActeurDTO() throws SQLException {
        int nacteur = acteurRs.getInt(1);
        String nom = acteurRs.getString(2);
        String prenom = acteurRs.getString(3);
        java.util.Date naissance = acteurRs.getDate(4);
        String nationalite = acteurRs.getString(5);
        int nbrFilms = acteurRs.getInt(6);

        return new ActeurDTO(nacteur, nom, prenom, naissance, nationalite, nbrFilms);
    }

    public int position() throws SQLException{
        return acteurRs.getRow();
    }

    public int getNbActeur() {
        return nbActeur;
    }
}
