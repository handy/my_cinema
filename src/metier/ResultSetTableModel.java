package metier;

import javax.swing.table.AbstractTableModel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

abstract class ResultSetTableModel extends AbstractTableModel {

    private ResultSet rs;
    private ResultSetMetaData rsmd;

    public ResultSet getResultSet() {
        return rs;
    }

    public ResultSetTableModel(ResultSet aResultSet) {
        rs = aResultSet;
        try {
            rsmd = rs.getMetaData();
        } catch (SQLException e) {
            System.out.println("Erreur " + e);
        }
    }

    @Override
    public String getColumnName(int column) {
        try {
            return rsmd.getColumnLabel(column+1).toUpperCase();
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public int getColumnCount() {
        try{
            return rsmd.getColumnCount();
        }
        catch (SQLException e)
        {
            System.out.println("Erreur " + e);
            return 0;
        }
    }
}
