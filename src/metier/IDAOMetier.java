package metier;

import DTO.ActeurDTO;
import DTO.ClientDTO;
import DTO.FilmDTO;
import DTO.GenreDTO;

import java.sql.SQLException;
import java.util.List;

public interface IDAOMetier {
    List<GenreDTO> ensGenres();

    long nbreFilmDuGenre(int unGenre);

    List<FilmDTO> ensFilmDuGenre(int unGenre);

    FilmDTO infoRealisateurEtActeur(int unFilm);

//    List<ActeurDTO> ensActeur();
//    TODO : implementer acteur;
//    List<FilmDTO> ensTitresDunActeur(int unActeur);

    List<FilmDTO> ensFilmEmpruntables();
    List<FilmDTO> ensFilmRestituable(int client);

    void createClient(String nom, String prenom, String adresse, int anciennete);

    List<ClientDTO> findAllClients();

    ClientDTO findClientById(int id);

    void deleteClientById(int id) throws SQLException;

    void updateClientById(int id);

    void emprunter(int nclient, int nfilm);

    void restituer(int nclient, int nfilm);


}
