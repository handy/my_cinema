package metier;

import DTO.ClientDTO;
import DTO.FilmDTO;
import DTO.GenreDTO;
import service.ConnectCinema;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DAOMetier implements IDAOMetier {

    private final static String SQLFindAllGenre =
            "SELECT ngenre, nature "
                    + "FROM GENRE "
                    + "ORDER BY nature";

    private final static String SQLFindNbFilmByGenre =
            "SELECT nature, count(nfilm) as nbr "
                    + "FROM FILM f, GENRE g "
                    + "WHERE f.ngenre = g.ngenre "
                    + "AND f.ngenre = ?";

    private final static String SQLFindAllFilmByGenre =
            "SELECT f.nfilm, f.titre, f.realisateur, a.nom "
                    + "FROM FILM f, GENRE g, ACTEUR a "
                    + "WHERE f.ngenre = g.ngenre "
                    + "AND f.ngenre = ? "
                    + "AND f.nacteurPrincipal = a.nacteur "
                    + "ORDER BY f.titre ";

    private final static String SQLFindFilmById =
            "SELECT f.nfilm, f.titre, f.realisateur, a.nom "
                    + "FROM FILM f, ACTEUR a "
                    + "WHERE f.nfilm = ? "
                    + "AND f.nacteurPrincipal = a.nacteur ";

    private final static String SQLFinfAllFilmsEmpruntables =
            "SELECT NFILM, TITRE, REALISATEUR as NOM_REALISATEUR, NOM as NOM_ACTEUR " +
                    "From FILM " +
                    "INNER JOIN ACTEUR " +
                    "ON NACTEURPRINCIPAL + NACTEUR" +
                    "WHERE not nfilm in " +
                    "(SELECT nfilm FROM emprunt WHERE retour = 'non') " +
                    "ORDER BY titre";

    private final static String SQLFinfAllFilmsRestituablesByClinet =
            "SELECT NFILM, TITRE, REALISATEUR as NOM_REALISATEUR, NOM as NOM_ACTEUR " +
                    "From FILM " +
                    "INNER JOIN ACTEUR " +
                    "ON NACTEURPRINCIPAL + NACTEUR" +
                    "WHERE nfilm in " +
                    "(SELECT nfilm FROM emprunt join client using(nclient) WHERE retour = 'non' and nclient = ? ) " +
                    "ORDER BY titre";

    private final static String SQLCreateClient =
            "INSERT INTO CLIENT(nom, prenom, adresse, anciennete) VALUES (?, ?, ?, ?)";

    private final static String SQLFindAllClient =
            "SELECT nclient, nom, prenom, adresse, anciennete "
                    + "FROM CLIENT "
                    + "ORDER BY nom";

    private final static String SQLDeleteClient =
            "DELETE FROM CLIENT WHERE nclient = ? ";

    private final static String SQLDeleteClientDansEmprunt =
            "DELETE FROM EMPRUNT WHERE nclient = ? ";

    private final static String SQLNbreEmpruntEnCours =
            "SELECT COUNT(*) AS nbre " +
                    "FROM CLIENT " +
                    "INNER JOIN EMPRUNT " +
                    "USING(nclient) " +
                    "WHERE retour = 'non' " +
                    "AND nclient = ? ";

    private final static String SQLUpdateClient =
            "UPDATE CLIENT " +
                    "SET adresse = ? " +
                    ",anciennete = ? " +
                    "WHERE nclient = ?";

    private final static String SQLFindClientById =
            "SELECT nclient, nom, prenom, adresse, anciennete "
                    + "FROM CLIENT " +
                    "WHERE nclient = ? "
                    + "ORDER BY nom";

    private final static String SQLEmprunter =
            "INSERT INTO EMPRUNT(nclient, nfilm, retour, dateEmprunt ) VALUES(?, ?, 'non', current_date )";

    private final static String SQLRestituer =
            "UPDATE EMPRUNT SET retour = 'oui' WHERE nclient = ? AND nfilm = ?";

    //SINGLETON precoce
    private static DAOMetier INSTANCE = new DAOMetier();

    public static IDAOMetier getInstance() {
        return INSTANCE;
    }

    @Override
    public List<GenreDTO> ensGenres() {
        List<GenreDTO> liste = new ArrayList<>();
        try {
            Statement instr =
                    ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLFindAllGenre);

            while (rs.next()) {
                int ngenre = rs.getInt(1);
                String nature = rs.getString("NATURE");

                liste.add(new GenreDTO(ngenre, nature));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public long nbreFilmDuGenre(int unGenre) {
        long resultat = -1L;
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindNbFilmByGenre);
            pstm.setInt(1, unGenre);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                resultat = rs.getLong("nbr");
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return resultat;
    }

    @Override
    public List<FilmDTO> ensFilmDuGenre(int unGenre) {
        List<FilmDTO> liste = new ArrayList<>();
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindAllFilmByGenre);
            pstm.setInt(1, unGenre);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nom_realisateur = rs.getString("realisateur");
                String nom_acteur = rs.getString("nom");

                liste.add(new FilmDTO(nfilm, titre, nom_realisateur, nom_acteur));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public FilmDTO infoRealisateurEtActeur(int unFilm) {
        FilmDTO film = null;
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindFilmById);
            pstm.setInt(1, unFilm);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nom_realisateur = rs.getString("realisateur");
                String nom_acteur = rs.getString("nom");

                film = new FilmDTO(nfilm, titre, nom_realisateur, nom_acteur);
            }
        } catch (
                SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return film;
    }

    @Override
    public List<FilmDTO> ensFilmEmpruntables() {
        List<FilmDTO> liste = new ArrayList<>();
        try {
            Statement instr =
                    ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLFinfAllFilmsEmpruntables);
            createListEmpruntableRestituable(liste, rs);
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public List<FilmDTO> ensFilmRestituable(int nclient) {
        List<FilmDTO> liste = new ArrayList<>();
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFinfAllFilmsRestituablesByClinet);
            pstm.setInt(1, nclient);
            ResultSet rs = pstm.executeQuery();
            createListEmpruntableRestituable(liste, rs);
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    public void createListEmpruntableRestituable(List<FilmDTO> liste, ResultSet rs) throws SQLException {
        while (rs.next()) {
            int nfilm = rs.getInt(1);
            String titre = rs.getString(2);
            String nomReal = rs.getString(3);
            String nomActeur = rs.getString(4);

            liste.add(new FilmDTO(nfilm, titre, nomReal, nomActeur));
        }
    }

    @Override
    public void createClient(String nom, String prenom, String adresse, int anciennete) {
        nom = (nom == null ? "GATOR" : nom.toUpperCase());
        prenom = (prenom == null ? "NATHALIE" : prenom.toUpperCase());
        adresse = (adresse == null ? "LENS" : adresse.toUpperCase());
        anciennete = (Math.max(anciennete, 0));

        Connection conn = ConnectCinema.getInstance();
        try {
            PreparedStatement ps = conn.prepareStatement(SQLCreateClient);

            ps.setString(1, nom);
            ps.setString(2, prenom);
            ps.setString(3, adresse);
            ps.setInt(4, anciennete);

            conn.setAutoCommit(false);
            ps.executeUpdate();
            conn.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<ClientDTO> findAllClients() {
        List<ClientDTO> liste = new ArrayList<>();
        try {
            Statement instr =
                    ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLFindAllClient);

            while (rs.next()) {
                int nclient = rs.getInt(1);
                String nom = rs.getString(2);
                String prenom = rs.getString(3);
                String adresse = rs.getString(4);
                int ancienete = rs.getInt(5);

                liste.add(new ClientDTO(nclient, nom, prenom, adresse, ancienete));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ClientDTO findClientById(int id) {
        ClientDTO client = null;
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindClientById);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                client = new ClientDTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return client;
    }

    @Override
    public void deleteClientById(int id) throws SQLException {
        Connection conn = ConnectCinema.getInstance();
        try {

            PreparedStatement pstmt = conn.prepareStatement(SQLNbreEmpruntEnCours);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                if (rs.getInt(1) > 0) {
                    System.out.println("impossible de supprimer le client");
                    return;
                }
            }
            System.out.println("suppression du client");

            pstmt = conn.prepareStatement(SQLDeleteClientDansEmprunt);
            pstmt.setInt(1, id);
            pstmt.addBatch();

            pstmt = conn.prepareStatement(SQLDeleteClient);
            pstmt.setInt(1, id);
            pstmt.addBatch();

            conn.setAutoCommit(false);
            int[] nbLignes = pstmt.executeBatch();
            conn.commit();



        } catch (Exception e) {
            e.printStackTrace();
            conn.rollback();

        }
    }

    @Override
    public void updateClientById(int id) {

    }

    @Override
    public void emprunter(int nclient, int nfilm) {
        Connection conn = ConnectCinema.getInstance();
        try {
            PreparedStatement ps = conn.prepareStatement(SQLEmprunter);

            ps.setInt(1, nclient);
            ps.setInt(2, nfilm);

            conn.setAutoCommit(false);
            ps.executeUpdate();
            conn.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void restituer(int nclient, int nfilm) {
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLRestituer);
            pstm.setInt(1, nclient);
            pstm.setInt(2, nfilm);

            conn.setAutoCommit(false);
            pstm.executeUpdate();
            conn.commit();

        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {

        DAOMetier dao = new DAOMetier();
        dao.deleteClientById(2);
    }
}
